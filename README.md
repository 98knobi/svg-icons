# SVG-Icons

Simple Webcomponents based wrapper for SVG Tag.

Enables you to to use SVG-based Icon-Packs like [Material Design Icons](https://materialdesignicons.com/).

This Package uses [Lit](https://lit.dev/) to build native Webcomponents, so it can be used with any Framework.

## Usage

Simply install the Package and import it, then you can use the component like any other html tag.

The tag expects the Icons SVG-Path as an attribute.

You can either provide it yourself as an String or use the [@mdi/js](https://www.npmjs.com/package/@mdi/js) Package.

### Lit Example

```ts
import 'wc-svg-icons';
import { mdiHandWave } from '@mdi/js';

@customElement('index-view')
export class IndexView extends LitElement {
  render() {
    return html`
      <h1>Hello World <svg-icon path=${mdiHandWave}></svg-icon></h1>
    `;
  }
}
```

## Notes

The Icons should behave like text.

They can easily be scaled or colored using css `color` and `font-size` options.