import { html } from 'lit';
import * as icons from '@mdi/js';

import '../dist/svg-icon';

export default {
  title: 'SVG Icon',
  argTypes: {
    color: { control: 'color' },
    iconName: {
      options: Object.keys(icons),
    },
    fontSize: { control: 'number' }
  },
};

const Template = ({ iconName, color, fontSize }) => {
  return html`
    <div style="color: ${color}; font-size: ${fontSize}px;">
      <svg-icon path=${icons[iconName]}></svg-icon>
    </div>
  `;
};

export const Icon = (args) => Template(args);
Icon.args = {
  iconName: 'mdiAccount',
  color: 'black',
  fontSize: 60,
};

