import { css, html, LitElement, svg } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('svg-icon')
export class SVGIcon extends LitElement {
  @property()
  path: string;

  private renderPath() {
    return svg`<path d="${this.path}" />`;
  }

  render() {
    return html`<svg viewBox="0 0 24 24">${this.renderPath()}</svg>`;
  }

  static get styles() {
    return css`
      :host {
        display: inline-flex;
        height: 1em;
        width: 1em;
        vertical-align: middle;
        pointer-events: none;
        align-items: center;
        justify-content: center;
      }

      svg {
        height: 100%;
        width: 100%;
      }

      svg path {
        fill: currentColor;
      }
    `;
  }
}
